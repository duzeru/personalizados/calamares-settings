# Calamares Settings

Calamares é uma estrutura de instalador genérica para distribuições Linux. Por padrão, ele contém um conjunto de palavras e imagens padrão. Este pacote fornece as ilustrações mais recentes do Debian, bem como scripts que suportam instalações EFI.

Ta